#diCarona-ws

Spring Rest WebService criado para atender as requisões do aplicativo de Carona.

Essa aplicação utiliza Spring Boot, para que seja executada, basta que:
Através do Maven:
mvn clean install spring-boot:run

Autor: Henrique Cipriano / hfcipriano@gmail.com