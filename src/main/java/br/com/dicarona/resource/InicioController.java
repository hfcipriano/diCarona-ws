package br.com.dicarona.resource;

import br.com.dicarona.exception.*;
import br.com.dicarona.model.Usuario;
import br.com.dicarona.model.persistence.IUsuarioDao;
import br.com.dicarona.resource.util.ApplicationUtil;
import br.com.dicarona.resource.util.AuthenticationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;

/**
 * Created by Henrique on 17/04/2016.
 */
@RestController
public class InicioController {
    @Autowired
    private IUsuarioDao usuarioDao;


    @RequestMapping("/")
    public String inicio(){
        throw new NotFoundException("Nao existe");
    }

    @RequestMapping("/login")
    @Transactional
    public Usuario login(String email, String senha, Long fbId, String fbToken, @RequestParam String notificacaoToken){
        try {
            if(fbId != null){
                URL url = new URL(String.format(AuthenticationUtil.VALIDADE_TOKEN_PATTERN, fbId, fbToken));
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                if(con.getResponseCode() == HttpURLConnection.HTTP_OK){

                    Usuario usuario = usuarioDao.findByFbid((Long) fbId);
                    if(usuario == null){
                        throw new UserNotRegistredException();
                    }
                    usuario.setToken(String.valueOf(Long.toHexString(new SecureRandom().nextLong())));
                    usuario.setNotificacaoToken(notificacaoToken);
                    return usuarioDao.save(usuario);
                }
                else{
                    throw new InvalidTokenException();
                }
            }
            else if(email != null){
                ApplicationUtil.validarEmail(email);
                Usuario usuario = usuarioDao.findByEmailAndSenha(email, senha);
                if(usuario == null){
                    throw new UserNotRegistredException();
                }
                usuario.setToken(String.valueOf(Long.toHexString(new SecureRandom().nextLong())));
                usuario.setNotificacaoToken(notificacaoToken);
                usuarioDao.save(usuario);
                return usuario;
            }
            throw new GenericException("Parametros inválios ou nulos");
        }catch (IOException e){
            throw new NotFoundException("Erro inesperado");
        }
    }
}
