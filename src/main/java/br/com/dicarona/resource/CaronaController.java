package br.com.dicarona.resource;

import br.com.dicarona.exception.GenericException;
import br.com.dicarona.exception.InvalidTokenException;
import br.com.dicarona.model.*;
import br.com.dicarona.model.persistence.*;
import br.com.dicarona.model.utils.NotificacaoEnum;
import br.com.dicarona.model.utils.StatusCaronaEnum;
import br.com.dicarona.model.utils.TipoUsuarioEnum;
import br.com.dicarona.model.utils.TipoVeiculoEnum;
import br.com.dicarona.resource.util.ApplicationUtil;
import br.com.dicarona.resource.util.AuthenticationUtil;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Henrique on 23/04/2016.
 */
@RestController
@RequestMapping("carona")
public class CaronaController {
    @Autowired
    private ICaronaDao caronaDao;

    @Autowired
    private IRotaDao rotaDao;

    @Autowired
    private ILocalDao localDao;

    @Autowired
    private IUsuarioDao usuarioDao;

    @Autowired
    private INotificacaoDao notificacaoDao;

    @Autowired
    private IAvaliacaoDao avaliacaoDao;

    @RequestMapping()
    @Transactional
    public Long novo(@RequestParam String token, @RequestParam double partidaLatitude, @RequestParam double partidaLongitude, @RequestParam double destinoLatitude, @RequestParam double destinoLongitude, @RequestParam String partidaEndereco, @RequestParam String destinoEndereco, Carona carona){
        try {
            Usuario usuario = validarToken(token);

            Rota rota = new Rota();
            rota.setPartida(localDao.save(new Local(partidaLatitude, partidaLongitude, partidaEndereco)));
            rota.setDestino(localDao.save(new Local(destinoLatitude, destinoLongitude, destinoEndereco)));

            carona.setMotorista(usuario);
            carona.setStatus(StatusCaronaEnum.AGUARDANDO_PASSAGEIRO);
            carona.setData(new Date());
            carona.setRota(rotaDao.save(rota));
            caronaDao.save(carona);
            return carona.getId();
        }catch (RuntimeException e){
            throw new GenericException(e.getLocalizedMessage());
        }
    }

    @RequestMapping("listar")
    @Transactional
    public List<Carona> listarCaronasDisponiveis(@RequestParam String token, double partidaLatitude, double partidaLongitude, double destinoLatitude, double destinoLongitude, double distanciaMaxima, TipoVeiculoEnum tipoVeiculo){
        try {
            Usuario usuarioCarona = validarToken(token);

            List<Carona> caronaList;
            if(tipoVeiculo.equals(TipoVeiculoEnum.TODOS)) {
                caronaList = caronaDao.findByStatusAndVagasGreaterThan(StatusCaronaEnum.AGUARDANDO_PASSAGEIRO, 0);
            }
            else{
                caronaList = caronaDao.findByStatusAndVagasGreaterThanAndVeiculo(StatusCaronaEnum.AGUARDANDO_PASSAGEIRO, 0, tipoVeiculo);
            }
            List<Carona> caronaPossivelList = new ArrayList<>();

            GeoApiContext geoApiContext = new GeoApiContext().setApiKey(AuthenticationUtil.GOOGLE_MAPS_SECRET);
            Iterator<Carona> iterador = caronaList.stream().filter(c -> !c.getPassageiros().contains(usuarioCarona)).iterator();
            while (iterador.hasNext()) {
                Carona carona = iterador.next();
                DirectionsApiRequest request = DirectionsApi.newRequest(geoApiContext).origin(carona.getRota().getPartida().toString()).destination(carona.getRota().getDestino().toString());

                DirectionsResult directionsResult = request.awaitIgnoreError();
                if (directionsResult.routes.length > 0 && directionsResult.routes[0].legs.length > 0) {
                    int[] distanciaEncontrada = {0};
                    if (ApplicationUtil.verificaProximidadeLocal(directionsResult.routes[0].legs[0].steps, new Local(partidaLatitude, partidaLongitude, null), new Local(destinoLatitude, destinoLongitude, null), distanciaMaxima, distanciaEncontrada)) {
                        carona.setDistanciaEncontrada(distanciaEncontrada[0]);
                        caronaPossivelList.add(carona);
                    }
                }
            }
            Collections.sort(caronaPossivelList);
            return caronaPossivelList;
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    /**
     * Solicita a particiáção da carona ao motorista. (Visão do passageiro)
     * @param token
     * @param caronaMotoristaId
     * @return Carona do motorista
     */
    @RequestMapping("solicitar")
    @Transactional
    public Carona solicitarCarona(@RequestParam String token, @RequestParam Long caronaMotoristaId){
        Usuario usuario  = validarToken(token);

        Carona caronaMotorista = caronaDao.findOne(caronaMotoristaId);
        if(caronaMotorista.getPassageiros().size() >= caronaMotorista.getVagas() && caronaMotorista.getVagas() <= 0){
            throw new GenericException("A carona já atingiu a quantidade máxima de passageiros.");
        }

        //Gera notificação ao passageiro
        Notificacao notificacao = new Notificacao();
        notificacao.setCarona(caronaMotorista);
        notificacao.setUsuario(usuario);
        notificacao.setStatus(NotificacaoEnum.CARONA_SOLICITADA);
        notificacao.setTipoUsuario(TipoUsuarioEnum.PASSAGEIRO);
        notificacao.setData(new Date());
        notificacaoDao.save(notificacao);
        ApplicationUtil.sendJson(caronaMotorista.getMotorista().getNotificacaoToken(), usuario.getNome() + " solicitou sua carona!");

        return caronaMotorista;
    }

    /**
     * Usuario motorista envia resposta de notificação
     * @param token
     * @param notificacaoId
     * @param aceitar true para adicionar o passageiro na carona, false para o contrário
     */
    @RequestMapping("notificar")
    @Transactional
    public void notificar(@RequestParam String token, @RequestParam Long notificacaoId, boolean aceitar){
        Usuario motorista = validarToken(token);

        Notificacao notificacao = notificacaoDao.findOne(notificacaoId);
        if(aceitar){
            if(notificacao.getCarona().getVagas() > 0) {
                notificacao.setStatus(NotificacaoEnum.CARONA_CONFIRMADA);
                notificacao.getCarona().getPassageiros().add(notificacao.getUsuario());
                notificacao.getCarona().setVagas(notificacao.getCarona().getVagas() - 1);
                notificacaoDao.save(notificacao);
                ApplicationUtil.sendJson(notificacao.getUsuario().getNotificacaoToken(), " Carona aceita!)");
            }
            else{
                notificacaoDao.delete(notificacao);
                throw new GenericException("A carona já atingiu a quantidade máxima de passageiros.");
            }
        }
        else{
            notificacao.setStatus(NotificacaoEnum.CARONA_NEGADA);
            notificacaoDao.save(notificacao);
            ApplicationUtil.sendJson(notificacao.getUsuario().getNotificacaoToken(), "Carona negada.");
        }
    }

    /**
     * Confirma a carona do motorista. Envia notificação à todos os passageiros
     * @param token Token de aceso do motorista
     * @param caronaMotoristaId Id da carona relacionada ao motorista
     */
    @RequestMapping("confirmar")
    @Transactional
    public void confirmarCarona(@RequestParam String token, @RequestParam Long caronaMotoristaId){
        validarToken(token);

        Carona carona = caronaDao.findOne(caronaMotoristaId);
        carona.setStatus(StatusCaronaEnum.CONCLUIDA);
        caronaDao.save(carona);

        for(Usuario passageiro : carona.getPassageiros()){
            ApplicationUtil.sendJson(
                    passageiro.getNotificacaoToken(),
                    carona.getMotorista().getNome() + " confirmou sua carona! Ligue " + carona.getMotorista().getTelefone()
            );
        }
    }


    /**
     * Busca a lista de Caronas pendentes.
     * @param token Token de acesso do usuario
     * @return Uma lista com 10 elementos filtrada pelo id do motorista, e pela negação do status, ordenada pela data
     */
    @RequestMapping("historico")
    public List<Carona> buscarHistorico(@RequestParam String token){
        Usuario usuario = validarToken(token);

        return caronaDao.findTop10ByMotoristaIdAndStatusNotAndStatusNotOrderByDataDesc(usuario.getId(), StatusCaronaEnum.CANCELADA, StatusCaronaEnum.CONCLUIDA);
    }

    /**
     * Passageiro busca as caronas pendentes
     * @param token
     * @return
     */
    @RequestMapping("notificacoesPendentes")
    public List<Notificacao> notificacoesPendentes(@RequestParam String token){
        Usuario usuario = validarToken(token);

        return notificacaoDao.findByStatusAndCaronaMotorista(NotificacaoEnum.CARONA_SOLICITADA, usuario).stream().filter(n -> n.getStatus().equals(NotificacaoEnum.CARONA_SOLICITADA)).collect(Collectors.toList());
    }

    @RequestMapping("obterPorUsuario")
    public List<Carona> obterCarona(Long id){
        try {
            return caronaDao.findByMotorista_Id(id);
        }catch (RuntimeException e){
            throw new GenericException(e.getMessage());
        }
    }

    @RequestMapping("cancelar")
    @Transactional
    public void cancelar(@RequestParam String token, @RequestParam Long caronaId){
        validarToken(token);
        Carona carona = caronaDao.findOne(caronaId);
        carona.setStatus(StatusCaronaEnum.CANCELADA);
        caronaDao.save(carona);
        notificacaoDao.delete(carona.getNotificacaoList());
    }

    @Deprecated
    @RequestMapping("concluir")
    public void concluir(@RequestParam String token, @RequestParam Long caronaId){
        validarToken(token);
        Carona carona = caronaDao.findOne(caronaId);
        carona.setStatus(StatusCaronaEnum.CONCLUIDA);
        caronaDao.save(carona);
    }

    public Usuario validarToken(String token){
        Usuario usuario = usuarioDao.findByToken(token);
        if(usuario == null){
            throw new InvalidTokenException();
        }
        return usuario;
    }
}
