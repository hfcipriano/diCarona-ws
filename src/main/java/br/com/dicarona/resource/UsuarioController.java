package br.com.dicarona.resource;

import br.com.dicarona.exception.GenericException;
import br.com.dicarona.exception.NotFoundException;
import br.com.dicarona.model.Usuario;
import br.com.dicarona.model.persistence.IUsuarioDao;
import br.com.dicarona.resource.util.ApplicationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Henrique on 21/04/2016.
 */
@RestController
@RequestMapping("usuario")
public class UsuarioController {

    @Autowired
    private IUsuarioDao usuarioDao;

    @RequestMapping("{id}")
    public Usuario obter(@PathVariable Long id){
        try {
            return usuarioDao.findOne(id);
        }catch (Exception e){
            System.out.println(e);
            throw new GenericException(e.getMessage());
        }
    }

    @RequestMapping("obterPorToken")
    public Usuario obter(@RequestParam String token){
        try {
            return usuarioDao.findByToken(token);
        }catch (Exception e){
            System.out.println(e);
            throw new GenericException(e.getMessage());
        }
    }

    @RequestMapping("salvar")
    @Transactional
    public Usuario salvar(Usuario usuario){
        try{
            if (usuario.getId() != null) {
                usuario.setToken(String.valueOf(Long.toHexString(new SecureRandom().nextLong())));
                return usuarioDao.save(usuario);
            } else {
                usuario.setInicio(new Timestamp(new Date().getTime()));
                usuario.setToken(String.valueOf(Long.toHexString(new SecureRandom().nextLong())));
                return usuarioDao.save(usuario);
            }
        }catch (JpaSystemException e){
            System.out.println(e.getCause().getCause().getMessage());
            throw new GenericException(e.getCause().getCause().getMessage());

        }
        catch (Exception e){
            System.out.println(e);
            throw new GenericException(e.getMessage());
        }
    }

    @RequestMapping("enviarMensagem")
    public void enviarMensagem(String mensagemS){
        if(StringUtils.isEmpty(mensagemS)){
            throw new NotFoundException("Não encontrado");
        }
        for(Usuario usuario : usuarioDao.findAll()){
            ApplicationUtil.sendJson(usuario.getNotificacaoToken(), mensagemS);
        }
    }
}
