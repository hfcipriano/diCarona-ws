package br.com.dicarona.resource.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Created by Henrique on 24/04/2016.
 */
public class AuthenticationUtil {
    public final static String JWT_SECRET = "19z31kkj23jkj12cmksg301lx13ml";
    public final static String GOOGLE_MAPS_SECRET = "AIzaSyC3PDgJVH9MPNnSmdTt6jIClfmNT7rjFAI";

    /*
    Pattern de validação do token de acesso do facebook.
    Variável 1: Facebook_id
    Variável 2: Token de acesso
     */
    public final static String VALIDADE_TOKEN_PATTERN = "https://graph.facebook.com/%s/permissions?access_token=%s";

    public static Claims generateMap(String encryptMessage){
        return Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(encryptMessage).getBody();
    }

    public static String tokenGenerator(String usuario, String senha, String fbId) {
        String token = Jwts.builder()
                .claim("email", usuario)
                .claim("senha", senha)
                .claim("fbId", fbId)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
        return token;
    }

    /*template static void main(String[] args) {
        String message = Jwts.builder().setPayload("{\"email\":\"henriquefc@hotmail.com.br\",\"senha\":\"1\"}").signWith(SignatureAlgorithm.HS512, AuthenticationUtil.JWT_SECRET).compact();

        System.out.println();

    }*/
}
