package br.com.dicarona.resource.util;

import br.com.dicarona.exception.InvalidDataException;
import br.com.dicarona.model.Local;
import com.google.gson.Gson;
import com.google.maps.model.DirectionsStep;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import java.util.regex.Pattern;

/**
 * Created by henrique on 01/05/16.
 */
public class ApplicationUtil {
    public final static String GCM_KEY = "AIzaSyCQwy41SV5Ups0uTbdgIBRFROCO94l2bEo";
    public final static String IONIC_PROFILE = "geral";
    private final static String IONIC_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1YmI5MTRlNy0wY2FiLTQwNDMtOTNhOC01MWQwYjJkZTg1ODYifQ.WDclF9j84nVtoJo-KCwoEqEZa0uns_X4Hc0jw1OpgMg";


    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static void validarEmail(String email){
        Pattern ptr = Pattern.compile(EMAIL_PATTERN);

        if(!ptr.matcher(email).matches()){
            throw new InvalidDataException("O campo email é inválido");
        }
    }

    public static void sendJson(final String token, final String mensagem) {
        Thread t = new Thread() {
            public void run() {
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 1000); //Timeout Limit
                HttpResponse response;
                Header[] headers = {new BasicHeader(HTTP.CONTENT_TYPE, "application/json"), new BasicHeader("Authorization", IONIC_TOKEN)};

                try {
                    HttpPost post = new HttpPost("https://api.ionic.io/push/notifications");
                    StringEntity se = new StringEntity(new Gson().toJson(new IonicMessage(new String[]{token}, IONIC_PROFILE, mensagem)));
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    post.setHeaders(headers);
                    response = client.execute(post);

                /*Checking response */
                    if (response != null) {
                        String retorno = EntityUtils.toString(response.getEntity());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
        t.start();
    }

    /**
     * Calcula a distância entre os locais do passageiro e todas as subrotas da possível carona
     * @param steps Objeto do Google Maps que contém a informação completa da Rota
     * @param partida Objeto que contém as coordenadas de partida do passageiro
     * @param destino Objeto que contém as coordenadas de destino do passageiro
     * @param distanciaMaxima Valor que dita o limite da distância entre as rotas
     * @return
     */
    public static Boolean verificaProximidadeLocal(DirectionsStep[] steps, Local partida, Local destino, double distanciaMaxima, int[] distanciaEncontrada) {
        Boolean partidaProxima = false;
        Boolean destinoProximo = false;
        for(DirectionsStep step : steps){
            //Valida se à distância da subRota para a partida é menor que a distanciaMaxima
            double distanciaPartida = DistanceCalculatorUtil.obterDistancia(step.startLocation.lat, step.startLocation.lng, partida.getLatitude(), partida.getLongitude());
            if(distanciaPartida <= distanciaMaxima){
                if(distanciaPartida > distanciaEncontrada[0]){
                    distanciaEncontrada[0] = (int) distanciaPartida;
                }
                partidaProxima = true;
            }
            //Valida se à distância da subRota o destino é menor que a distanciaMaxima
            double distanciaDestino = DistanceCalculatorUtil.obterDistancia(step.startLocation.lat, step.startLocation.lng, destino.getLatitude(), destino.getLongitude());
            if(distanciaDestino <= distanciaMaxima){
                if(distanciaDestino > distanciaEncontrada[0]){
                    distanciaEncontrada[0] = (int) distanciaDestino;
                }
                destinoProximo = true;
            }

            if(partidaProxima && destinoProximo){
                return true;
            }
        }
        return false;
    }
}
