package br.com.dicarona.resource.util;

/**
 * Created by Henrique on 12/05/2016.
 */
public class IonicMessage {
    private String[] tokens;
    private String profile;
    private Notification notification;

    public IonicMessage(String[] tokens, String profile, String message) {
        this.tokens = tokens;
        this.profile = profile;
        this.notification = new Notification(message);
    }

    private class Notification {
        private String message;

        public Notification(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public String[] getTokens() {
        return tokens;
    }

    public void setTokens(String[] tokens) {
        this.tokens = tokens;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }
}
