package br.com.dicarona.model.utils;

/**
 * Created by Henrique on 16/05/2016.
 */
public enum NotificacaoEnum {
    CARONA_SOLICITADA("CS"),
    CARONA_CONFIRMADA("CC"),
    CARONA_NEGADA("CN");

    private String id;

    NotificacaoEnum(String status) {
        this.id = status;
    }

    public String getId() {
        return id;
    }
}
