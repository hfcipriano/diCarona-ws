package br.com.dicarona.model.utils;

/**
 * Created by Henrique on 23/04/2016.
 */
public enum StatusCaronaEnum {
    AGUARDANDO_MOTORISTA ("AM"),
    AGUARDANDO_PASSAGEIRO("AP"),
    AGENDADA("agendada"),
    CANCELADA("cancelada"),
    CONCLUIDA("concluida");

    private String id;

    StatusCaronaEnum(String status) {
        this.id = status;
    }

    public String getId() {
        return id;
    }
}
