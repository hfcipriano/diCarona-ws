package br.com.dicarona.model.utils;

/**
 * Created by Henrique on 23/04/2016.
 */
public enum TipoUsuarioEnum {
    MOTORISTA ("MOTORISTA"),
    PASSAGEIRO ("PASSAGEIRO");

    private String id;

    TipoUsuarioEnum(String usuario) {
        this.id = usuario;
    }

    public String getId() {
        return id;
    }
}
