package br.com.dicarona.model.utils;

/**
 * Created by Henrique on 08/05/2016.
 */
public enum TipoVeiculoEnum {
    TODOS("TODOS"),
    CARRO("CARRO"),
    MOTO("MOTO");

    private String veiculo;

    TipoVeiculoEnum(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getVeiculo() {
        return veiculo;
    }
}
