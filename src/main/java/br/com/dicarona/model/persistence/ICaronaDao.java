package br.com.dicarona.model.persistence;

import br.com.dicarona.model.Carona;
import br.com.dicarona.model.utils.StatusCaronaEnum;
import br.com.dicarona.model.utils.TipoVeiculoEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Henrique on 22/04/2016.
 */
@Transactional
public interface ICaronaDao extends CrudRepository<Carona, Long> {
    @Query("select c from Carona c where c.motorista.id = :id")
    public List<Carona> findByMotorista_Id(@Param("id") Long Id);

    public List<Carona> findByStatus(StatusCaronaEnum status);

    public List<Carona> findByStatusAndVagasGreaterThan(StatusCaronaEnum status, Integer vagas);

    public List<Carona> findByStatusAndVagasGreaterThanAndVeiculo(StatusCaronaEnum status, Integer vagas, TipoVeiculoEnum tipoVeiculo);

    public List<Carona> findTop10ByMotoristaIdAndStatusNotAndStatusNotOrderByDataDesc(Long id, StatusCaronaEnum statusN, StatusCaronaEnum status);
}
