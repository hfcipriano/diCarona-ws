package br.com.dicarona.model.persistence;

import br.com.dicarona.model.Rota;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Henrique on 23/04/2016.
 */
@Transactional
public interface IRotaDao extends CrudRepository<Rota, Long>{
}
