package br.com.dicarona.model.persistence;

import br.com.dicarona.model.Notificacao;
import br.com.dicarona.model.Usuario;
import br.com.dicarona.model.utils.NotificacaoEnum;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Henrique on 16/05/2016.
 */
@Transactional
public interface INotificacaoDao extends CrudRepository<Notificacao, Long> {
    public List<Notificacao> findByUsuarioAndStatus(Usuario usuario, NotificacaoEnum status);

    public List<Notificacao> findByStatusAndCaronaMotorista(NotificacaoEnum status, Usuario motorista);
}
