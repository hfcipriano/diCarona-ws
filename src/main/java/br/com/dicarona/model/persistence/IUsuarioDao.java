package br.com.dicarona.model.persistence;

import br.com.dicarona.model.Carona;
import br.com.dicarona.model.Usuario;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by Henrique on 21/04/2016.
 */

@Transactional
public interface IUsuarioDao extends CrudRepository<Usuario, Long> {
   public Usuario findByFbid(Long fbId);
   public Usuario findByEmailAndSenha(String email, String senha);
   public Usuario findByToken(String token);
}
