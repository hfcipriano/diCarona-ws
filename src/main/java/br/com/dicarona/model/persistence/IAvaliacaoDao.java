package br.com.dicarona.model.persistence;

import br.com.dicarona.model.Avaliacao;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Henrique on 16/05/2016.
 */
@Transactional
public interface IAvaliacaoDao extends CrudRepository<Avaliacao, Long> {
}
