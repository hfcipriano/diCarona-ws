package br.com.dicarona.model.persistence;

import br.com.dicarona.model.Local;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by Henrique on 23/04/2016.
 */
@Transactional
public interface ILocalDao extends CrudRepository<Local, Long>{
}
