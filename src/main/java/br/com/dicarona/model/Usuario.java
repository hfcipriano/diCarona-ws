/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dicarona.model;

import br.com.dicarona.model.utils.StatusCaronaEnum;
import com.fasterxml.jackson.annotation.*;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Henrique
 */
@Entity
@Table(name = "USUARIO")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")})
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "fb_id")
    @JsonIgnore
    private Long fbid;
    @Size(max = 255)
    @Column(name = "nome")
    private String nome;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "senha")
    @JsonIgnore
    private String senha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicio;
    @Size(max = 45)
    @Column(name = "telefone")
    private String telefone;
    @Lob
    @Column(name = "foto")
    private byte[] foto;
    @Size(max = 700)
    @Column(name = "token")
    private String token;
    @Size(max = 500)
    @Column(name = "notificacao_Token")
    private String notificacaoToken;
    @OneToMany(mappedBy = "motorista")
    @JsonIgnore
    private List<Carona> motorista = new ArrayList<>();
    @JoinTable(name = "CARONA_PASSAGEIROS", joinColumns = {
            @JoinColumn(name = "CARONA_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "USUARIO_id", referencedColumnName = "id")})
    @ManyToMany
    @JsonIgnore
    private List<Carona> passageiros = new ArrayList<>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
    @JsonIgnore
    private List<Avaliacao> avaliacaoList = new ArrayList<>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
    @JsonIgnore
    private List<Notificacao> notificacaoList = new ArrayList<>();



    public Usuario() {
    }

    public Usuario(Long id) {
        this.id = id;
    }

    public Usuario(Long id, Date inicio) {
        this.id = id;
        this.inicio = inicio;
    }

    @JsonGetter
    public Double getMediaAvaliacao(){
        Double media = 0D;
        for(Avaliacao avaliacao : avaliacaoList){
            media += avaliacao.getNota();
        }
        if(avaliacaoList.size() > 0) {
            return media / new Double(avaliacaoList.size());
        }
        return media;
    }


    @JsonGetter
    public List<Long> getCaronasComoMotorista(){
        List<Long> caronaIds = new ArrayList<>();
        motorista.stream().filter(carona -> carona.getStatus().equals(StatusCaronaEnum.AGUARDANDO_PASSAGEIRO)).forEach(c -> caronaIds.add(c.getId()));
        return caronaIds;
    }

    @JsonGetter
    public List<Long> getCaronasComoPassageiro(){
        List<Long> caronaIds = new ArrayList<>();
        motorista.stream().filter(carona -> carona.getStatus().equals(StatusCaronaEnum.AGUARDANDO_MOTORISTA)).forEach(c -> caronaIds.add(c.getId()));
        return caronaIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFbid() {
        return fbid;
    }

    public void setFbid(Long fbid) {
        this.fbid = fbid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNotificacaoToken() {
        return notificacaoToken;
    }

    public void setNotificacaoToken(String notificacaoToken) {
        this.notificacaoToken = notificacaoToken;
    }

    public List<Carona> getMotorista() {
        return motorista;
    }

    public void setMotorista(List<Carona> motorista) {
        this.motorista = motorista;
    }

    public List<Carona> getPassageiro() {
        return passageiros;
    }

    public void setPassageiro(List<Carona> passageiros) {
        this.passageiros = passageiros;
    }

    public List<Avaliacao> getAvaliacaoList() {
        return avaliacaoList;
    }

    public void setAvaliacaoList(List<Avaliacao> avaliacaoList) {
        this.avaliacaoList = avaliacaoList;
    }

    public List<Notificacao> getNotificacaoList() {
        return notificacaoList;
    }

    public void setNotificacaoList(List<Notificacao> notificacaoList) {
        this.notificacaoList = notificacaoList;
    }

    public List<Carona> getPassageiros() {
        return passageiros;
    }

    public void setPassageiros(List<Carona> passageiros) {
        this.passageiros = passageiros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.dicarona.model.Usuario[ id=" + id + " ]";
    }
    
}
