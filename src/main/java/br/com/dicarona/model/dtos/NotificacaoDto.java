package br.com.dicarona.model.dtos;

import br.com.dicarona.model.Notificacao;
import br.com.dicarona.model.utils.TipoUsuarioEnum;

/**
 * Created by Henrique on 16/05/2016.
 */
@Deprecated
public class NotificacaoDto {
    private TipoUsuarioEnum tipoUsuario;

    public NotificacaoDto(Notificacao notificacao) {
        if(notificacao.getUsuario().getId().equals(notificacao.getCarona().getMotorista().getId())){
            tipoUsuario = TipoUsuarioEnum.MOTORISTA;
        }
        else{
            tipoUsuario = TipoUsuarioEnum.PASSAGEIRO;
        }
    }

    public TipoUsuarioEnum getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
}
