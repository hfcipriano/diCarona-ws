package br.com.dicarona.model.dtos;

import br.com.dicarona.model.Usuario;

import java.math.BigInteger;

/**
 * Created by Henrique on 21/04/2016.
 */
public class UsuarioDto {
    private Long id;
    private String nome;
    private String telefone;
    private double avaliacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(double avaliacao) {
        this.avaliacao = avaliacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
