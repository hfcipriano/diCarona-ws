package br.com.dicarona.model;

import br.com.dicarona.model.utils.NotificacaoEnum;
import br.com.dicarona.model.utils.TipoUsuarioEnum;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Henrique on 16/05/2016.
 */
@Entity
@Table(name = "NOTIFICACAO")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
@NamedQueries({
        @NamedQuery(name = "Notificacao.findAll", query = "SELECT n FROM Notificacao n")})
public class Notificacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private NotificacaoEnum status;
    @Column(name = "data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipoUsuario")
    private TipoUsuarioEnum tipoUsuario;
    @JoinColumn(name = "carona", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Carona carona;
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Usuario usuario;

    public Notificacao() {
    }

    public Notificacao(Long id) {
        this.id = id;
    }

    public Notificacao(Long id, NotificacaoEnum status, TipoUsuarioEnum tipoUsuario) {
        this.id = id;
        this.status = status;
        this.tipoUsuario = tipoUsuario;
    }

    @JsonGetter
    public String getNome(){
        return usuario.getNome();
    }

    @JsonGetter
    public String getDestinoNome(){
        return carona.getRota().getDestino().getEndereco();
    }

    @JsonGetter
    public String getMotoristaNome(){
        return carona.getMotorista().getNome();
    }
    @JsonGetter
    public Long getMotoristaId(){
        return carona.getMotorista().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NotificacaoEnum getStatus() {
        return status;
    }

    public void setStatus(NotificacaoEnum status) {
        this.status = status;
    }

    public Carona getCarona() {
        return carona;
    }

    public void setCarona(Carona carona) {
        this.carona = carona;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public TipoUsuarioEnum getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacao)) {
            return false;
        }
        Notificacao other = (Notificacao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.dicarona.model.Notificacao[ id=" + id + " ]";
    }
}
