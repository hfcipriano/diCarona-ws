/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dicarona.model;

import br.com.dicarona.model.dtos.UsuarioDto;
import br.com.dicarona.model.utils.StatusCaronaEnum;
import br.com.dicarona.model.utils.TipoVeiculoEnum;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Henrique
 */
@Entity
@Table(name = "CARONA")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
@NamedQueries({
    @NamedQuery(name = "Carona.findAll", query = "SELECT c FROM Carona c")})
public class Carona implements Serializable, Comparable<Carona> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private StatusCaronaEnum status;
    @JoinColumn(name = "rota", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Rota rota;
    @JoinColumn(name = "motorista", referencedColumnName = "id")
    @ManyToOne
    @JsonIgnore
    private Usuario motorista;
    @JoinTable(name = "CARONA_PASSAGEIROS", joinColumns = {
            @JoinColumn(name = "CARONA_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "USUARIO_id", referencedColumnName = "id")})
    @ManyToMany
    @JsonIgnore
    private List<Usuario> passageiros = new ArrayList<>();
    @NotNull
    @Column(name = "vagas")
    private Integer vagas;
    @NotNull
    @Column(name = "tipoVeiculo")
    private TipoVeiculoEnum veiculo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carona")
    @JsonIgnore
    private List<Notificacao> notificacaoList = new ArrayList<>();

    @Transient int distanciaEncontrada;

    public Carona() {
    }

    public Carona(Long id) {
        this.id = id;
    }

    public Carona(Long id, Date data, StatusCaronaEnum status) {
        this.id = id;
        this.data = data;
        this.status = status;
    }

    @JsonGetter
    public UsuarioDto getMotoristaInfo(){
        UsuarioDto usuarioDto = new UsuarioDto();
        if(motorista != null) {
            usuarioDto.setId(motorista.getId());
            usuarioDto.setNome(motorista.getNome());
            usuarioDto.setTelefone(motorista.getTelefone());
            usuarioDto.setAvaliacao(motorista.getMediaAvaliacao());
        }
        return usuarioDto;
    }

    public int getDistanciaEncontrada(){
        return distanciaEncontrada;
    }

    @JsonGetter
    public int getPassageirosAssociados(){
        return passageiros.size();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public StatusCaronaEnum getStatus() {
        return status;
    }

    public void setStatus(StatusCaronaEnum status) {
        this.status = status;
    }

    public Rota getRota() {
        return rota;
    }

    public void setRota(Rota rota) {
        this.rota = rota;
    }

    public Usuario getMotorista() {
        return motorista;
    }

    public void setMotorista(Usuario motorista) {
        this.motorista = motorista;
    }

    public List<Usuario> getPassageiros() {
        return passageiros;
    }

    public void setPassageiros(List<Usuario> passageiros) {
        this.passageiros = passageiros;
    }

    public Integer getVagas() {
        return vagas;
    }

    public void setVagas(Integer vagas) {
        this.vagas = vagas;
    }

    public TipoVeiculoEnum getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(TipoVeiculoEnum veiculo) {
        this.veiculo = veiculo;
    }

    public List<Notificacao> getNotificacaoList() {
        return notificacaoList;
    }

    public void setNotificacaoList(List<Notificacao> notificacaoList) {
        this.notificacaoList = notificacaoList;
    }

    public void setDistanciaEncontrada(int distanciaEncontrada) {
        this.distanciaEncontrada = distanciaEncontrada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carona)) {
            return false;
        }
        Carona other = (Carona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.dicarona.model.Carona[ id=" + id + " ]";
    }

    @Override
    public int compareTo(Carona o) {
        int distanceCompare = ((Carona) o).getDistanciaEncontrada();

        return this.getDistanciaEncontrada() - distanceCompare;
    }
}
