package br.com.dicarona.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Henrique on 24/04/2016.
 */
@ResponseStatus(value= HttpStatus.UNAUTHORIZED, reason="Falha na autenticação")
public class AuthenticationException extends RuntimeException {
    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException() {
    }
}
