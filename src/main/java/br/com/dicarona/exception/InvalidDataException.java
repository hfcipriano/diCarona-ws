package br.com.dicarona.exception;

/**
 * Created by henrique on 01/05/16.
 */
public class InvalidDataException extends GenericException {
    public InvalidDataException(String msg) {
        super(msg);
    }
}
