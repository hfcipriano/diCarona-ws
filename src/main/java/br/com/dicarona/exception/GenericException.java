package br.com.dicarona.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Henrique on 21/04/2016.
 */

@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
public class GenericException extends RuntimeException{
    public GenericException(String msg) {
        super(msg);
    }
}
