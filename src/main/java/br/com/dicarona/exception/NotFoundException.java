package br.com.dicarona.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Henrique on 17/04/2016.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Nao encontrado")
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
