package br.com.dicarona.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Henrique on 24/04/2016.
 */
@ResponseStatus(value= HttpStatus.PRECONDITION_FAILED)
public class UserNotRegistredException extends GenericException{
    public UserNotRegistredException() {
        super("Usuário não registrado na base local");
    }
}
