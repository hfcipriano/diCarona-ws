package br.com.dicarona.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Henrique on 18/04/2016.
 */
@ResponseStatus(value= HttpStatus.UNAUTHORIZED)
public class InvalidTokenException extends GenericException {
    public InvalidTokenException() {
        super("Token invalido");
    }
}
